#!/bin/bash

REMOTE_USERNAME="root"
REMOTE_HOST="51.254.39.205"

docker build -t sayyeahlo:portfolio .
docker save -o archive.zip sayyeahlo:portfolio

rsync -azhe ssh --progress docker-compose.yml root@51.254.39.205:bertrandcandas.com/
rsync -azhe ssh --progress archive.zip root@51.254.39.205:tmp/

ssh -tt $REMOTE_USERNAME@$REMOTE_HOST << EOF
docker rm -f sayyeahlo:portfolio || true
docker load -i tmp/archive.zip
cd bertrandcandas.com
docker-compose up -d

exit
EOF

rm ./archive.zip
