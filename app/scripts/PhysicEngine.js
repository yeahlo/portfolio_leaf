var CANNON = require('cannon.js');

var PhysicsEngine = function (scene,world,camera) {
	this.world = new CANNON.World();
	this.world.broadphase = new CANNON.NaiveBroadphase();
	this.world.solver.iterations = 15;
	this.world.gravity.set(0,-40,0);
	this.world.quatNormalizeFast = false;
	this.world.quatNormalizeSkip = 0;
	this.sphereShape = new CANNON.Sphere(0.5);
	this.lastCallTime = 0;
}

PhysicsEngine.prototype.update = function () {

    var timeStep = 1 / 60;
    var now = Date.now() / 1000;

    if(!this.lastCallTime){
        this.world.step(timeStep);
        lastCallTime = now;
        return;
    }

    var timeSinceLastCall = now - this.lastCallTime;
    this.world.step(timeStep, timeSinceLastCall,3);
    lastCallTime = now;
    
}

PhysicsEngine.prototype.getWorld = function () {
	return this.world;
}

PhysicsEngine.prototype.resize = function() {
    this.world.clearForces();
}


module.exports = PhysicsEngine;