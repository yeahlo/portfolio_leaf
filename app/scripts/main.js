var THREE = require('three.js');
var RendererEngine = require('./Renderer.js');
var Leaf = require('./leaf.js');
var PhysicEngine = require('./PhysicEngine.js');
var ContentManager = require('./ContentManager.js');
var MobileDetect = require('mobile-detect');
var renderEngine;
var contentManager;
var leafs = [];
var md = new MobileDetect(window.navigator.userAgent);
var desktop = false;
init();
animate();

function init() {

  if(md.mobile() || md.tablet()) {
    desktop = false;
    document.documentElement.className += 'touch';
  }else {
    desktop = true;
  }

  renderEngine = new RendererEngine();
  physicEngine = new PhysicEngine();
  contentManager = new ContentManager(renderEngine.getScene(),physicEngine.getWorld(),renderEngine.getCamera(),desktop);

  var loader = new THREE.JSONLoader();
  loader.load('./models/leaf.json',onLoad);

  window.addEventListener( 'resize', onWindowResize, false );
  onWindowResize();

  document.querySelector('#mail').setAttribute('href','mailto:' + 'cfafnfdfasbefrtrafnd@gmfaiflf.fcfom'.replace(/f/g,''));

}

function onLoad(geometry, materials) {

  var loader = new THREE.TextureLoader();

  var map = loader.load('./images/leaf.jpg', function () {
    renderEngine.show();
  });

  var alphaMap = loader.load('./images/alpha.jpg');
  var specularMap = loader.load('./images/specular.jpg');

  if(params.debug) {
    m = new THREE.MeshBasicMaterial({
      color : 0x2194ce,
      wireframe : true
    });
  }else {


    if(desktop) {

      m = new THREE.MeshPhongMaterial( {
       side: THREE.DoubleSide,
       alphaTest : .5,
       transparent : true,
       shininess : 10,
       specularMap : specularMap,
       map : map,
       alphaMap : alphaMap
     });

    }else {

      m = new THREE.MeshPhongMaterial( {
        side: THREE.DoubleSide,
        alphaTest : .5,
        transparent : true,
        shininess : 10,
        map : map,
        alphaMap : alphaMap,
      });

    }
  }

  if(desktop) {

    leafs.push(new Leaf(-2,2.9,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.1,false,1,.6));
    leafs.push(new Leaf(-.8,1,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.1,false,1,.7));
    leafs.push(new Leaf(2,3.5,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.15,false,1,.75));
    leafs.push(new Leaf(.5,4.9,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.15,false,1,.7));
    leafs.push(new Leaf(1,-.5,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.1,false,1,.8));
    leafs.push(new Leaf(-2,1.7,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,0,false,1));
    leafs.push(new Leaf(2,2.25,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.1));
    leafs.push(new Leaf(-2.3,3,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.1));
    leafs.push(new Leaf(.8,1,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.2));
    leafs.push(new Leaf(-1.2,0,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.1));
    leafs.push(new Leaf(.5,3.8,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.1));
    leafs.push(new Leaf(3.8,-.2,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,0));
    leafs.push(new Leaf(3.5,3.5,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.1));
    leafs.push(new Leaf(-3,4.3,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.2));
    leafs.push(new Leaf(3,4.5,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.2));
    leafs.push(new Leaf(-5,1,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.3,true,.9));
    leafs.push(new Leaf(-1.5,5.3,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.37,true,.8));
    leafs.push(new Leaf(5,1.2,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.25,true,.85));
    leafs.push(new Leaf(-6.5,4.4,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.05,true,.75));

  }else {

    leafs.push(new Leaf(-1,1.7,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.2));
    leafs.push(new Leaf(1,.2,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,0));
    leafs.push(new Leaf(0,3,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,.2));
    leafs.push(new Leaf(0,3.5,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.2));
    leafs.push(new Leaf(1,4.3,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.05));
    leafs.push(new Leaf(.2,4.7,renderEngine.getScene(),physicEngine.getWorld(),geometry,desktop,m,-.15));

  }

  var loader = new THREE.TextureLoader();
  loader.load('./images/leaf.jpg', function () {
    renderEngine.show();
  });

}

function animate() {
  requestAnimationFrame( animate );

  renderEngine.update();
  contentManager.update();

  if(!contentManager.isVideoPlaying()) {
    physicEngine.update();
  }

  var l = leafs.length;
  while(l--) {
    leafs[l].update();
  }

}

function onWindowResize() {
  renderEngine.resize(window.innerWidth,window.innerHeight);

  contentManager.resize();

  var l = leafs.length;

  while(l--) {
    leafs[l].resize();
  }

  physicEngine.resize();
}

