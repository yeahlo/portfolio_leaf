var PlayerManager = function (desktop) {

	this.links = [];
	this.desktop = desktop;
	this.isPlaying = false;

	if (document.readyState != 'loading'){
		this.init();
	} else {
		document.addEventListener('DOMContentLoaded', this.init.bind( this ),false);
	}

}

PlayerManager.prototype.init = function () {
	
	document.removeEventListener('DOMContentLoaded', this.init);
	this.links = document.querySelectorAll('a');
}

PlayerManager.prototype.isVideoPlaying = function () {
	return this.isPlaying;
}

PlayerManager.prototype.stopAll = function (e) {
	
	for (var i = 0 ; i < this.links.length ; i++) {
		this.stopPlayer(this.links[i]);
	}

	this.isPlaying = false;

}

PlayerManager.prototype.startPlayer = function (target) {
	

	if(this.desktop) {

		var video = target.querySelector('video');

		if(!video) {
			video = document.createElement('video'); 
			video.setAttribute('loop', true);
			video.setAttribute('repeat', true);
			video.setAttribute('muted', true);
			video.setAttribute('autoplay', true);
			target.appendChild(video);
		}

		if(!video.src.length) {
			video.src = './video/' + target.dataset.video; 
		}

		video.play();

		this.isPlaying = true;

	}else {
		
		if(!target.querySelector('img')) {
			var img = document.createElement('img'); 
			img.setAttribute('src', './video/' + target.dataset.image);
			var w = img.offsetWidth;
			target.appendChild(img);
		}
		
	}
	
	setTimeout(function () {
		target.classList.add('on-hover');
	},0);
	
}

PlayerManager.prototype.stopPlayer = function (target) {
	target.classList.remove('on-hover');
	var video = target.querySelector('video');
	if(video) {
		video.pause(); 
		this.isPlaying = false;
	}
}

module.exports = PlayerManager;