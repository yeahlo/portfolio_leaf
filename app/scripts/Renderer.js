var THREE = require('three.js');
//var dat = require('dat-gui');
var Renderer = function () {

	this.position = new THREE.Vector3(-10,0,0);
	this.rotation = new THREE.Vector3(0,0,0);
	this.lightIntensity = 0;
	this.shown = false;
	
	this.camera = new THREE.PerspectiveCamera( 30, 1, 1, 10000 );
	this.camera.position.z = 10;
	this.camera.position.y = 2;
	this.scene = new THREE.Scene();
	this.scene.fog = new THREE.Fog( 0x12141e, -15 ,12);//
	this.camera.useTarget = false;

	this.renderer = new THREE.WebGLRenderer( { antialias: true } );
	this.renderer.setClearColor( 0x12141e );
	this.renderer.setPixelRatio(1);

	this.light = new THREE.AmbientLight( 0xffffff,.7); //.5 soft white light
	this.scene.add( this.light );

	this.deepLight = new THREE.PointLight(0x39aeff, 1, 170);
	this.deepLight.position.set( 30, 10, 150 );
	this.scene.add(this.deepLight);
	
	this.bottomLight = new THREE.PointLight(0x8216a0, 1, 50);
	this.bottomLight.position.set( 0, -10, 20 );
	this.scene.add(this.bottomLight);

	this.specularLight = new THREE.PointLight(0xffe221, 1, 100);
	this.specularLight.position.set( 1.2, 10.3, -10 );
	this.scene.add(this.specularLight);
	
	document.body.appendChild( this.renderer.domElement );
}


Renderer.prototype.show = function () {
	this.shown = true;
}

Renderer.prototype.update = function () {

	this.renderer.render( this.scene, this.camera );

	
	if(this.shown && this.scene.fog.near < 5) {
		this.scene.fog.near += .08;
	}

}




Renderer.prototype.getScene = function () {
	return this.scene;
}

Renderer.prototype.getCamera = function () {
	return this.camera;
}

Renderer.prototype.resize = function (w,h) {

	this.renderer.setSize( w, h );
	this.camera.aspect = w / h;

	this.camera.updateMatrixWorld();
	this.camera.updateProjectionMatrix();
};

module.exports = Renderer;


