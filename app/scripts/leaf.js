var Leaf = function (x,y,scene,world,geometry,desktop,m,rotation,static,scale,globalScale) {

	this.globalScale = globalScale || .95 + (Math.random() * .1 - Math.random() * .1);
	this.desktop = desktop;
	this.static = static || false;
	this.scale = scale || 1;
	this.bodies = [];
	this.meshs = [];
	this.skinnedMesh = new THREE.SkinnedMesh(geometry, m);
	this.skinnedMesh.scale.set(this.globalScale,this.globalScale,this.globalScale);
	scene.add(this.skinnedMesh);

	this.skinnedMesh.material.skinning = true;
	this.skinnedMesh.position.x = x;
	this.skinnedMesh.position.z = -2.5;
	this.skinnedMesh.position.y = y;
	this.skinnedMesh.rotation.y = rotation;
	this.skinnedMesh.rotation.x = .07;
	this.skinnedMesh.bind( this.skinnedMesh.skeleton );

	if(!this.static) {

		var size = .5;
		var material = new THREE.MeshLambertMaterial( { color: 0xffffff } );

		var space = 0.3*size;
		var N = this.skinnedMesh.skeleton.bones.length;
		var last;
		var space;

		var bodies = [];

		for (var i = 0 ; i < N ; i++) {

			var sphereShape = new CANNON.Box(new CANNON.Vec3(1 * this.globalScale,.1 * this.globalScale,.45 * this.globalScale));

			var sphere_geometry = new THREE.BoxGeometry(  sphereShape.halfExtents.x*2,
				sphereShape.halfExtents.y*2,
				sphereShape.halfExtents.z*2 );

			var spherebody = new CANNON.Body({
				mass: i == 0 ? 0 : .05 + 10 * i + Math.random() * 1 * i *  this.globalScale,
				shape: sphereShape,
				position: new CANNON.Vec3(this.skinnedMesh.position.x + i * rotation * this.globalScale,this.skinnedMesh.position.y + .5 * this.globalScale,this.skinnedMesh.position.z + i * 1.4 * this.globalScale )//1.4
			});

			spherebody.initVelocity.set(Math.random(500),Math.random(500),Math.random(500));

			bodies.push(spherebody);

			world.addBody(spherebody);

			var mesh = new THREE.Mesh( sphere_geometry,material);

			var o = spherebody.position;
			var q = spherebody.quaternion;

			mesh.position.set(o.x, o.y, o.z);
			mesh.quaternion.set(q.x, q.y, q.z, q.w);

			if(params.debug) {
				scene.add(mesh);
			}

			this.bodies.push(spherebody);
			this.meshs.push(mesh);

			if(last){
				var c = new CANNON.LockConstraint(spherebody, last);
				world.addConstraint(c);
			}

			last = spherebody;
		}

		/*
		setInterval(function () {
			for (var i = 0 ; i < bodies.length ; i++) {
				bodies[i].applyLocalImpulse(new CANNON.Vec3(50,50,50),new CANNON.Vec3());
			}

		},Math.random() * 3000 + 500);
		*/

	}else {

		for (var i = 0 ; i < this.skinnedMesh.skeleton.bones.length ; i++) {
			this.skinnedMesh.skeleton.bones[i].rotation.x += .12 * i;
		}
	}





}

Leaf.prototype.resize = function() {
}

Leaf.prototype.update = function () {

	if(this.static) {
		return;
	}

	for (var i = 0 ; i < this.skinnedMesh.skeleton.bones.length ; i++) {

		this.meshs[i].position.copy(this.bodies[i].position);

		if(this.bodies[i].quaternion){
			this.bodies[i].quaternion.y = Math.max(-.1,Math.min(.1,this.bodies[i].quaternion.y));
			this.bodies[i].quaternion.z = Math.max(-.1,Math.min(.1,this.bodies[i].quaternion.z));

			this.meshs[i].quaternion.copy(this.bodies[i].quaternion);
		}

		var o = this.bodies[i].position;
		var q = this.bodies[i].quaternion;

		this.skinnedMesh.skeleton.bones[i].quaternion.set(q.x * .5, q.y, q.z, q.w);

	}

}

module.exports = Leaf;
