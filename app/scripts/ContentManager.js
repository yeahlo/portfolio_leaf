var CANNON = require('cannon.js');
var _ = require('lodash');
var PlayerManager = require('./PlayerManager.js');

var ContentManager = function (scene,world,camera,desktop) {


	this.camera = camera;
	this.world = world;
	this.scene = scene;
	this.lastScrollY = 0;
	this.bodies = [];
	this.mesh = [];
	this.divs = [];
	this.player;
	this.idTimeoutEvent = -1;
	this.idTimeoutAutoPlay = -1;
	this.desktop = desktop;
	this.container = document.querySelector('main');
	this.step = 0;
	this.removeMouseEvent = false;
	this.playerManager = new PlayerManager(desktop);

	this.scrollerHidden = false;

	if (document.readyState != 'loading'){
		this.init();
	} else {
		document.addEventListener('DOMContentLoaded', this.init.bind( this ),false);
	}

}

ContentManager.prototype.init = function () {

	document.removeEventListener('DOMContentLoaded', this.init);

	this.divs = document.querySelectorAll('.phys');

	for (var i = 0 ; i < this.divs.length ; i++) {
		this.createPhys(this.divs[i]);
	}

	this.createBodies();
}

ContentManager.prototype.isVideoPlaying = function() {
	return this.playerManager.isVideoPlaying();
}

ContentManager.prototype.checkScroll = function() {

	if(!this.removeMouseEvent) {
		this.removeMouseEvent = true;
		this.container.classList.add('no-event');
		this.playerManager.stopAll();
	}

	var that = this;
	clearTimeout(this.idTimeoutEvent);
	clearTimeout(this.idTimeoutAutoPlay);
	this.idTimeoutEvent = setTimeout(function () {
		that.container.classList.remove('no-event');
		that.removeMouseEvent = false;
		this.idTimeoutAutoPlay = setTimeout(that.checkCurrentPlayer(),1000);
	},350);

}

ContentManager.prototype.checkCurrentPlayer = function() {

	for (var i = 0 ; i < this.divs.length ; i++) {
		if(this.divs[i].getBoundingClientRect().top >  -this.divs[i].getBoundingClientRect().height * .5 && this.divs[i].getBoundingClientRect().top < window.innerHeight - this.divs[i].getBoundingClientRect().height * .5) {
			this.playerManager.startPlayer(this.divs[i]);
		}
	}
}

ContentManager.prototype.createPhys = function() {

	var geometry = new THREE.BoxGeometry( 1, 1, 1 );
	for ( var i = 0; i < geometry.faces.length; i += 2 ) {
		var hex = Math.random() * 0xffffff;
		geometry.faces[ i ].color.setHex( hex );
		geometry.faces[ i + 1 ].color.setHex( hex );
	}

	var material = new THREE.MeshBasicMaterial( {vertexColors: THREE.FaceColors} );
	cube = new THREE.Mesh( geometry, material );

	if(params.debug) {
		this.scene.add(cube);
	}
	this.mesh.push(cube);

};

ContentManager.prototype.screenTo3D = function (v) {

	var vector = new THREE.Vector3();

	vector.set(

		( v.x / window.innerWidth ) * 2 - 1,
		- ( v.y / window.innerHeight ) * 2 + 1,
		.5
		);

	vector.unproject( this.camera );

	var dir = vector.sub( this.camera.position ).normalize();
	var distance = - this.camera.position.z / dir.z;
	var pos = this.camera.position.clone().add( dir.multiplyScalar( distance ) );

	return pos;
}

ContentManager.prototype.createBodies = function () {

	this.bodies = [];

	var elt;
	var topLeft,bottomRight,w,h;


	for (var i = 0 ; i < this.divs.length ; i++) {

		elt = this.divs[i];

		topLeft = new THREE.Vector3(0,0,0);
		topLeft = this.screenTo3D(topLeft);

		bottomRight = new THREE.Vector3(elt.offsetWidth,elt.offsetHeight,0);
		bottomRight = this.screenTo3D(bottomRight);

		w = bottomRight.x - topLeft.x;
		h = topLeft.y - bottomRight.y;

		var boxShape = new CANNON.Box(new CANNON.Vec3(w/2,h/2,.5))
		boxShape._width = w/2;
		boxShape._height = h/2;
		var boxbody = new CANNON.Body({
			mass: 0,
			shape: boxShape,
			position: new CANNON.Vec3(0,0,.5)
		});

		this.world.addBody(boxbody);
		this.bodies.push(boxbody);
	}

}

ContentManager.prototype.resize = function() {

	if(this.divs.length) {
		var elt;
		var topLeft,bottomRight,w,h;
		var l = this.bodies.length;

		while(l--) {
			elt = this.divs[l];
			var b = this.bodies[l];

			topLeft = new THREE.Vector3(0,0,0);
			topLeft = this.screenTo3D(topLeft);

			bottomRight = new THREE.Vector3(elt.offsetWidth,elt.offsetHeight ,0);
			bottomRight = this.screenTo3D(bottomRight);

			w = (bottomRight.x - topLeft.x)/2;
			h = (topLeft.y - bottomRight.y)/2;

			b.shapes[0].halfExtents.x = w;
			b.shapes[0].halfExtents.y = h;

			b.shapes[0].boundingSphereRadiusNeedsUpdate = true;
			b.shapes[0].updateConvexPolyhedronRepresentation();
		}

	}

	/*
	if(this.divs.length) {
		var l = this.bodies.length;
		while(l--) {
			var b = this.bodies.splice(-1,1);

			console.log('body to remove',b);
			this.world.remove(b);
			b = null;
		}
		console.log('this.bodies',this.bodies.length)
		this.createBodies();
		//this.world.remove();
	}
	*/
}

ContentManager.prototype.update = function () {

	if(!this.bodies.length) {
		return;
	}

	var position,elt,w,h,topLeft;
	var top = this.container.scrollTop;

	if(top !== this.lastScrollY) {
		this.checkScroll();
	}

	var i = this.divs.length;

	while(i--) {

		elt = this.divs[i];

		position = new THREE.Vector3(elt.offsetLeft,elt.offsetTop - top,0);
		position = this.screenTo3D(position);

		topLeft = new THREE.Vector3(0,0,0);
		topLeft = this.screenTo3D(topLeft);

		bottomRight = new THREE.Vector3(elt.offsetWidth,elt.offsetHeight,0);
		bottomRight = this.screenTo3D(bottomRight);

		w = bottomRight.x - topLeft.x;
		h = topLeft.y - bottomRight.y;

		this.bodies[i].position.x = position.x + w/2;
		this.bodies[i].position.y = position.y + h/2 - h;

		var o = this.bodies[i].position;
		var q = this.bodies[i].quaternion;
		var s = this.bodies[i].shapes[0].halfExtents;

		this.mesh[i].position.set(o.x, o.y, o.z);
		this.mesh[i].quaternion.set(q.x, q.y, q.z, q.w);
		this.mesh[i].scale.set(s.x * 2,s.y * 2,s.z);

	}


	this.step ++;
	this.lastScrollY = top;

	if(top > 50 && !this.scrollerHidden) {
			this.scrollerHidden = true;
			document.querySelector('#scroller').classList += 'scrolled';
	}

}

module.exports = ContentManager;
